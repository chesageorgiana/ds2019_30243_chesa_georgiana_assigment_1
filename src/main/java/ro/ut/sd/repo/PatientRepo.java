package ro.ut.sd.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.Patient;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatientRepo extends JpaRepository<Patient, String> {

    Optional<Patient> findById(String id);

    List<Patient> findByCaregiver(Caregiver caregiver);

    Patient findByMedicalRecordIdIn(List<String> ids);

}
