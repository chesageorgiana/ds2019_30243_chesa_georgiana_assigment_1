package ro.ut.sd.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.ut.sd.model.Medication;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationRepo extends JpaRepository<Medication, String> {

    List<Medication> findAll();

    Optional<Medication> findById(String id);
}
