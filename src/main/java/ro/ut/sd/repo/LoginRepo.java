package ro.ut.sd.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.ut.sd.model.LoginModel;


@Repository
public interface LoginRepo extends JpaRepository<LoginModel, String> {

    LoginModel findByUsernameAndPassword(String username, String password);
}
