package ro.ut.sd.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.ut.sd.model.Caregiver;

import java.util.Optional;

@Repository
public interface CaregiverRepo extends JpaRepository<Caregiver, String> {

    Optional<Caregiver> findById(String id);

}
