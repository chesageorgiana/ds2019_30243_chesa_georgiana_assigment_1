package ro.ut.sd.responsedata;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * This class will provide ResponseEntity for all REST endpoints.
 */
public class ApiResponseFactory {

    private ApiResponseFactory(){}

    public static ResponseEntity buildErrorResponseMessage(HttpStatus status, String message) {
        return ResponseEntity.status(status).body(MessageResponse.builder().error_description(message).severity(ApiSeverity.DANGER.getValue()).build());
    }

    public static ResponseEntity buildErrorResponseMessage(HttpStatus status, String message, ApiSeverity severity) {
        return ResponseEntity.status(status).body(MessageResponse.builder().error_description(message).severity(severity.getValue()).build());
    }

    public static ResponseEntity buildSuccessResponseMessage(String message) {
        return ResponseEntity.status(HttpStatus.OK).body(MessageResponse.builder().severity(ApiSeverity.SUCCESS.getValue()).message(message).build());
    }

    public static ResponseEntity buildSuccessResponseMessage(Object body) {
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }

}
