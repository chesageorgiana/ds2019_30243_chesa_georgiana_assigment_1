package ro.ut.sd.responsedata;

import java.util.Arrays;

public enum ApiSeverity {


    SUCCESS("success"), DANGER("danger"), WARNING("warning"), PRIMARY("primary");

    private final String value;

    ApiSeverity(final String newValue) {
        value = newValue;
    }

    /**
     * Returns the associated {@link ApiSeverity} for the given String.
     *
     * @param enumString
     * @return associated ApiStatus for the given String
     */
    public static ApiSeverity fromString(String enumString) {
        return Arrays.stream(values())
                .filter(en -> en.getValue().equalsIgnoreCase(enumString))
                .findAny().orElse(null);
    }

    public String getValue() {
        return value;
    }

}
