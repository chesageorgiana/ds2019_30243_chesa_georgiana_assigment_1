package ro.ut.sd.responsedata;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * This class is used by API response factory to create a response only with a message.
 * If the request was performed in the right way, it will provide a message.
 * Otherwise, if an error occurred, an error message will be returned.
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageResponse {

    private String message;
    private String error_description;
    private String severity;
    private String id;

}
