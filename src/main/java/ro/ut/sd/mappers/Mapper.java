package ro.ut.sd.mappers;

import org.apache.commons.collections4.CollectionUtils;
import ro.ut.sd.dto.*;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.*;
import ro.ut.sd.utils.AppDateUtils;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Mapper {

    private Mapper() {
    }

    public static CaregiverDto mapUserToCaregiverDto(Caregiver caregiver) {
        return CaregiverDto.builder()
                .name(caregiver.getName())
                .id(caregiver.getId())
                .username(caregiver.getUsername())
                .birthDate(AppDateUtils.getStringFromDateWithDMYFormat(caregiver.getBirthDate()))
                .address(caregiver.getAddress())
                .gender(caregiver.getGender())
                .build();
    }

    public static List<PatientDto> mapUsersToPatientPartialDto(Collection<Patient> patients) {
        return patients.stream()
                .map(patient -> PatientDto.builder()
                        .id(patient.getId())
                        .gender(patient.getGender())
                        .name(patient.getName())
                        .build())
                .collect(Collectors.toList());
    }

    public static PatientDto mapUserToPatientDto(Patient patient) {
        return PatientDto.builder()
                .name(patient.getName()).username(patient.getUsername()).id(patient.getId())
                .imgUrl(patient.getImgUrl()).birthDate(AppDateUtils.getStringFromDateWithDMYFormat(patient.getBirthDate()))
                .address(patient.getAddress()).gender(patient.getGender())
                .build();
    }

    public static PatientDto mapUserToDetailedPatientDto(Patient patient) {
        PatientDto patientDto = PatientDto.builder()
                .name(patient.getName()).id(patient.getId()).imgUrl(patient.getImgUrl())
                .username(patient.getUsername())
                .birthDate(AppDateUtils.getStringFromDateWithDMYFormat(patient.getBirthDate()))
                .address(patient.getAddress()).gender(patient.getGender())
                .build();
        if (patient.getCaregiver() != null) {
            patientDto.setCaregiver(CaregiverDto.builder()
                    .name(patient.getCaregiver().getName())
                    .username(patient.getCaregiver().getUsername())
                    .id(patient.getId())
                    .build());
        }
        if (CollectionUtils.isNotEmpty(patient.getMedicalRecord())) {
            patientDto.setMedicalRecord(patient.getMedicalRecord().stream().map(m -> m.getName() + "(" + m.getDosage() + ")").collect(Collectors.toList()));
        }
        return patientDto;
    }

    public static MedicationDto mapMedicationToMedicationDto(Medication medication) {
        return MedicationDto.builder()
                .dosage(medication.getDosage()).id(medication.getId())
                .name(medication.getName()).sideEffects(medication.getSideEffects())
                .build();
    }

    public static List<MedicationDto> mapMedicationListToMedicationDtoList(List<Medication> medications) {
        return medications.stream().map(Mapper::mapMedicationToMedicationDto).collect(Collectors.toList());
    }

    public static List<CaregiverDto> mapUserListToCaregiverDtoList(List<Caregiver> users) {
        return users.stream().map(Mapper::mapUserToCaregiverDto).collect(Collectors.toList());
    }

    public static List<PatientDto> mapUserListToPatientsDtoList(List<Patient> users) {
        return users.stream().map(Mapper::mapUserToPatientDto).collect(Collectors.toList());
    }
}
