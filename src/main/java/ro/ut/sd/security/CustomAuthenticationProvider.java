package ro.ut.sd.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import ro.ut.sd.model.*;
import ro.ut.sd.service.LoginService;

import java.util.Collections;

@Component
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private LoginService loginService;

    @Override
    public Authentication authenticate(Authentication authentication) throws BadCredentialsException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        LoginModel loginModel = loginService.getByUsernameAndPassword(username, password);
        if(loginModel==null){
            throw new BadCredentialsException("Invalid login credentials! Please try again.");
        }
        return new UsernamePasswordAuthenticationToken(loginModel, password, Collections.singletonList(new SimpleGrantedAuthority(loginModel.getUser_type())));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}