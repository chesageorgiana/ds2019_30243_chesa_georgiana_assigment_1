package ro.ut.sd.security;

import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;

import ro.ut.sd.responsedata.ApiSeverity;
import ro.ut.sd.responsedata.MessageResponse;

import java.util.Date;


@Configuration
@EnableResourceServer
@Slf4j
public class OAuthPermissionConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.requestMatchers().antMatchers("/patient/**").and().authorizeRequests().antMatchers("/patient/**").permitAll();
        http.requestMatchers().antMatchers("/medication/**").and().authorizeRequests().antMatchers("/medication/**").permitAll();
        http.requestMatchers().antMatchers("/caregiver/**").and().authorizeRequests().antMatchers("/caregiver/**").permitAll();
        http.requestMatchers().antMatchers("/auth/**").and().authorizeRequests().antMatchers("/auth/**").permitAll();
        http.exceptionHandling()
                .authenticationEntryPoint((request, response, e) ->
                {
                    MessageResponse errorMessage = MessageResponse.builder().severity(ApiSeverity.DANGER.getValue())
                            .error_description("You do not have permission or access rights to the requested endpoint.").build();
                    log.error("Attempt to make a request for an endpoint with an invalid role/permission. Timestamp: " + new Date());
                    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    response.setStatus(HttpStatus.FORBIDDEN.value());
                    response.getWriter().write(new GsonBuilder().create().toJson(errorMessage));
                })
                .accessDeniedHandler((request, response, e) ->
                {
                    MessageResponse errorMessage = MessageResponse.builder().severity(ApiSeverity.DANGER.getValue())
                            .error_description("You do not have permission or access rights to the requested endpoint.").build();
                    log.error("Attempt to make a request for an endpoint with an invalid role/permission. Timestamp: " + new Date());
                    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    response.setStatus(HttpStatus.FORBIDDEN.value());
                    response.getWriter().write(new GsonBuilder().create().toJson(errorMessage));
                });
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        OAuth2AuthenticationEntryPoint authenticationEntryPoint = new OAuth2AuthenticationEntryPoint();
        authenticationEntryPoint.setExceptionTranslator(new CustomWebResponseExceptionTranslator());
        resources.authenticationEntryPoint(authenticationEntryPoint);
    }

    private class CustomWebResponseExceptionTranslator extends DefaultWebResponseExceptionTranslator {
        @Override
        public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
            ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
            OAuth2Exception body = responseEntity.getBody();
            HttpHeaders headers = new HttpHeaders();
            headers.setAll(responseEntity.getHeaders().toSingleValueMap());
            if (StringUtils.isNotEmpty(body.getMessage()) && body.getMessage().contains("Invalid access token:")) {
                body.addAdditionalInformation("error_description", "");
            }
            body.addAdditionalInformation("severity", ApiSeverity.DANGER.getValue());
            return new ResponseEntity<>(body, headers, responseEntity.getStatusCode());
        }
    }
}
