package ro.ut.sd.security;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import ro.ut.sd.model.Doctor;
import ro.ut.sd.model.LoginModel;
import ro.ut.sd.model.User;
import ro.ut.sd.responsedata.ApiSeverity;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String, Object> additionalInfo = new HashMap<>();
        LoginModel user = (LoginModel) authentication.getPrincipal();
        additionalInfo.put("username", user.getUsername());
        additionalInfo.put("role", user.getUser_type());
        additionalInfo.put("severity", ApiSeverity.SUCCESS.getValue());
        additionalInfo.put("fullname", user.getName());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
