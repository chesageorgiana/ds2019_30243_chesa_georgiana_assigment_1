package ro.ut.sd.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

@Slf4j
public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... objects) {
        log.error("Exception thrown in method: " + method.getName());
        log.error(throwable.getMessage());
        log.error("Parameters");
        for (Object param : objects) {
            log.error(param.toString());
        }
        log.error("Full stack trace");
        throwable.printStackTrace();
    }
}