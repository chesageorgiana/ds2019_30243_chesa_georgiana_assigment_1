package ro.ut.sd.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * This class will be used to change the format of any Date type.
 */
public class AppDateUtils {

    private AppDateUtils(){}

    public static String getStringFromDateWithDMYFormat(Date dateToConvert) {
        DateFormat dateFormatDMY = new SimpleDateFormat("dd.MM.yyyy");
        if (dateToConvert != null) {
            return dateFormatDMY.format(dateToConvert);
        }
        return null;
    }

    public static String getStringFromDateWithDMYTimeFormat(Date dateToConvert) {
        DateFormat dateFormatDMYTime = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        if (dateToConvert != null) {
            return dateFormatDMYTime.format(dateToConvert);
        }
        return null;
    }

    public static Date getExpireDate(int numberOfValidDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, numberOfValidDays);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    public static long getDaysDifferenceBetweenTwoDates(Date date1, Date date2) {
        long diffInMilliseconds = Math.abs(date2.getTime() - date1.getTime());
        return TimeUnit.DAYS.convert(diffInMilliseconds, TimeUnit.MILLISECONDS);
    }

}
