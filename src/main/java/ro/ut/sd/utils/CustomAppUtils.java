package ro.ut.sd.utils;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * This class will provide methods that can be generically used in any class of the application.
 */
public class CustomAppUtils {

    private static final Random random = new Random();

    private CustomAppUtils() {
    }

    public static long getDaysDifferenceBetweenTwoDates(Date date1, Date date2) {
        long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
        return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static Random getRandomInstance() {
        return random;
    }

    public static double getDoubleValueWithFourDecimals(double valueToBeConverted) {
        return Double.parseDouble(new DecimalFormat("#.####").format(valueToBeConverted));
    }

    public static String formatDoubleValueToPrettyValuesForQuiz(double d) {
        if (d == (long) d)
            return Long.toString((long) d);
        else
            return new DecimalFormat("#.##").format(d);
    }

}
