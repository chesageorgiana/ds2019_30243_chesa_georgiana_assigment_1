package ro.ut.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.Patient;
import ro.ut.sd.repo.PatientRepo;

import java.util.Arrays;
import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientRepo patientRepo;

    public List<Patient> getAll() {
        return patientRepo.findAll();
    }

    public Patient getById(String id) {
        return patientRepo.findById(id).orElse(null);
    }

    public List<Patient> getByCaregiver(Caregiver caregiver){
        return patientRepo.findByCaregiver(caregiver);
    }

    public void add(Patient patient){
        patientRepo.save(patient);
    }

    public void update(Patient patient){
        patientRepo.save(patient);
    }

    public void updateAll(List<Patient> patients){
        patientRepo.saveAll(patients);
    }

    public void remove(Patient patient){
        patientRepo.delete(patient);
    }

    public Patient getByMedicineId(String id){
        return patientRepo.findByMedicalRecordIdIn(Arrays.asList(id));
    }
}
