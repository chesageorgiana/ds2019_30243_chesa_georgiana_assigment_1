package ro.ut.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ut.sd.model.LoginModel;
import ro.ut.sd.repo.LoginRepo;

@Service
public class LoginService {

    @Autowired
    private LoginRepo loginRepo;

    public LoginModel getByUsernameAndPassword(String username, String password) {
        return loginRepo.findByUsernameAndPassword(username, password);
    }
}
