package ro.ut.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ut.sd.model.Medication;
import ro.ut.sd.repo.MedicationRepo;

import java.util.List;

@Service
public class MedicationService {

    @Autowired
    private MedicationRepo medicationRepo;

    public Medication getById(String id) {
        return medicationRepo.findById(id).orElse(null);
    }

    public List<Medication> getAll() {
        return medicationRepo.findAll();
    }

    public void addNew(Medication medication) {
        medicationRepo.save(medication);
    }

    public void update(Medication medication) {
        medicationRepo.save(medication);
    }

    public void remove(Medication medication) {
        medicationRepo.delete(medication);
    }

}
