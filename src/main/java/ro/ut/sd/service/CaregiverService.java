package ro.ut.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.Patient;
import ro.ut.sd.model.User;
import ro.ut.sd.repo.CaregiverRepo;

import java.util.List;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepo caregiverRepo;

    public Caregiver getById(String id) {
        return caregiverRepo.findById(id).orElse(null);
    }

    public List<Caregiver> getAll() {
        return caregiverRepo.findAll();
    }

    public void addNew(Caregiver caregiver) {
        caregiverRepo.save(caregiver);
    }

    public void update(Caregiver caregiver) {
        caregiverRepo.save(caregiver);
    }

    public void remove(Caregiver caregiver){
        caregiverRepo.delete(caregiver);
    }
}
