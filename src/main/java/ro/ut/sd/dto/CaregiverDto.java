package ro.ut.sd.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaregiverDto {


    private String id;

    private String birthDate;

    private String gender;

    private String address;

    private String name;

    private String username;

    private String password;

    private List<PatientDto> patients;

    private String message;

}
