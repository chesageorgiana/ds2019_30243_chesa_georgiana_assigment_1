package ro.ut.sd.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SessionUserDto {


    private String id;
    private String username;
    private String name;
    private String password;
    private String email;
    private String address;
    private String gender;
    private String birth_day;
    private String user_type;
    private String imgUrl;
}
