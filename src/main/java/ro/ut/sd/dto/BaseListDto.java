package ro.ut.sd.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * This class represents a DTO class of Doctor entity. It has a List of DoctorDTO
 * and a request message used to show a message to the APIs.
 */
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseListDto {

    private List<?> list;
    private String message;
}
