package ro.ut.sd.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatientDto {

    private String id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private List<String> medicalRecord;
    private String username;
    private String password;
    private String imgUrl;
    private CaregiverDto caregiver;

    private String message;
}
