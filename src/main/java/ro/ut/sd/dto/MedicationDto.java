package ro.ut.sd.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicationDto {

    private String id;
    private String name;
    private String sideEffects;
    private String dosage;

    private String message;
    private String patientId;
    private String medicationId;
}
