package ro.ut.sd.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This class represents a DTO class of Doctor entity. It has all the fields from the
 * User and a request message used to show a message to the APIs.
 */
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DoctorDto {

    private String id;
    private String username;
    private String name;
    private String email;

}
