package ro.ut.sd.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ut.sd.dto.BaseListDto;
import ro.ut.sd.dto.CaregiverDto;
import ro.ut.sd.dto.MedicationDto;
import ro.ut.sd.dto.PatientDto;
import ro.ut.sd.mappers.Mapper;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.Patient;
import ro.ut.sd.responsedata.ApiResponseFactory;
import ro.ut.sd.responsedata.ApiSeverity;
import ro.ut.sd.responsedata.MessageResponse;
import ro.ut.sd.service.CaregiverService;
import ro.ut.sd.service.PatientService;

import javax.ws.rs.Consumes;
import javax.ws.rs.PathParam;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping(value = "/caregiver", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private PatientService patientService;

    @GetMapping("/all")
    public ResponseEntity getAll() {
        List<Caregiver> allCaregivers = caregiverService.getAll();
        List<CaregiverDto> all = Mapper.mapUserListToCaregiverDtoList(allCaregivers);
        return ApiResponseFactory.buildSuccessResponseMessage(BaseListDto.builder().message("Successfully retrieved all Caregivers.").list(all).build());
    }

    @GetMapping("/details/{id}")
    public ResponseEntity getById(@PathVariable("id") String id) {
        Caregiver caregiver = caregiverService.getById(id);
        List<Patient> patients = patientService.getByCaregiver(caregiver);
        CaregiverDto caregiverDto = Mapper.mapUserToCaregiverDto(caregiver);
        caregiverDto.setPatients(patients.stream().map(p -> PatientDto.builder().id(p.getId()).name(p.getName()).build()).collect(Collectors.toList()));
        return ApiResponseFactory.buildSuccessResponseMessage(caregiverDto);
    }

    @PostMapping("/add")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addMedication(@RequestBody CaregiverDto caregiverDto) {
        Caregiver caregiver = Caregiver.builder()
                .address(caregiverDto.getAddress())
                .birthDate(new Date())
                .gender(caregiverDto.getGender())
                .name(caregiverDto.getName()).password(caregiverDto.getPassword())
                .username(caregiverDto.getUsername()).build();
        caregiverService.addNew(caregiver);
        return ApiResponseFactory.buildSuccessResponseMessage(MessageResponse.builder().severity(ApiSeverity.SUCCESS.getValue())
                .message("Successfully saved new caregiver " + caregiver.getName()).id(caregiver.getId()).build());
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity remove(@PathVariable("id") String id) {
        Caregiver caregiver = caregiverService.getById(id);
        List<Patient> allPatientsOfCaregiver = patientService.getByCaregiver(caregiver);
        allPatientsOfCaregiver.forEach(p->p.setCaregiver(null));
        patientService.updateAll(allPatientsOfCaregiver);
        caregiverService.remove(caregiver);
        return ApiResponseFactory.buildSuccessResponseMessage("Successfully removed caregiver "+caregiver.getName());
    }
}
