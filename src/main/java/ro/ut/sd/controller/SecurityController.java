package ro.ut.sd.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ro.ut.sd.dto.SessionUserDto;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.LoginModel;
import ro.ut.sd.model.Patient;
import ro.ut.sd.responsedata.ApiResponseFactory;
import ro.ut.sd.service.CaregiverService;
import ro.ut.sd.service.PatientService;
import ro.ut.sd.utils.AppDateUtils;

import javax.ws.rs.QueryParam;

@RestController()
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class SecurityController {

    @Autowired
    private PatientController patientController;

    @Autowired
    private CaregiverController caregiverController;

    @GetMapping("/remove/{token}")
    public ResponseEntity logout(@PathVariable("token") String token) {
        return ApiResponseFactory.buildSuccessResponseMessage("Logout success! Token removed "+ token);
    }

    @PostMapping("/session")
    public ResponseEntity getSessionUser(@QueryParam("token") String token) {
        LoginModel sessionUser = (LoginModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(sessionUser.getUser_type().equalsIgnoreCase("PATIENT")){
            return patientController.getById(sessionUser.getId());
        }
        if(sessionUser.getUser_type().equalsIgnoreCase("CAREGIVER")){
            return caregiverController.getById(sessionUser.getId());
        }
        return null;
    }
}
