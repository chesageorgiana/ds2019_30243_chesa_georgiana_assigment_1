package ro.ut.sd.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ut.sd.dto.BaseListDto;
import ro.ut.sd.dto.CaregiverDto;
import ro.ut.sd.dto.PatientDto;
import ro.ut.sd.mappers.Mapper;
import ro.ut.sd.model.Caregiver;
import ro.ut.sd.model.Patient;
import ro.ut.sd.responsedata.ApiResponseFactory;
import ro.ut.sd.responsedata.ApiSeverity;
import ro.ut.sd.responsedata.MessageResponse;
import ro.ut.sd.service.CaregiverService;
import ro.ut.sd.service.PatientService;

import javax.ws.rs.Consumes;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@RestController()
@RequestMapping(value = "/patient", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class PatientController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private CaregiverService caregiverService;

    @GetMapping("/all")
    public ResponseEntity getAll() {
        List<Patient> allCaregivers = patientService.getAll();
        List<PatientDto> all = Mapper.mapUserListToPatientsDtoList(allCaregivers);
        return ApiResponseFactory.buildSuccessResponseMessage
                (BaseListDto.builder().message("Successfully retrieved all patients.").list(all).build());
    }

    @GetMapping("/details/{id}")
    public ResponseEntity getById(@PathVariable("id") String id) {
        Patient patient = patientService.getById(id);
        PatientDto patientDto = Mapper.mapUserToDetailedPatientDto(patient);
        return ApiResponseFactory.buildSuccessResponseMessage(patientDto);
    }

    @PostMapping("/add")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addPatient(@RequestBody PatientDto patientDto) {
        Patient patient = Patient.builder()
                .address(patientDto.getAddress())
                .birthDate(new Date()).username(patientDto.getUsername())
                .password(patientDto.getPassword())
                .id(UUID.randomUUID().toString())
                .imgUrl(patientDto.getImgUrl())
                .name(patientDto.getName())
                .gender(patientDto.getGender())
                .build();
        patientService.add(patient);
        return ApiResponseFactory.buildSuccessResponseMessage(MessageResponse.builder().severity(ApiSeverity.SUCCESS.getValue())
                .message("Successfully saved new patient " + patient.getName()).id(patient.getId()).build());
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity remove(@PathVariable("id") String id) {
        Patient patient = patientService.getById(id);
        patientService.remove(patient);
        return ApiResponseFactory.buildSuccessResponseMessage("Successfully removed patient "+patient.getName());
    }
}
