package ro.ut.sd.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ut.sd.dto.BaseListDto;
import ro.ut.sd.dto.MedicationDto;
import ro.ut.sd.mappers.Mapper;
import ro.ut.sd.model.Medication;
import ro.ut.sd.model.Patient;
import ro.ut.sd.responsedata.ApiResponseFactory;
import ro.ut.sd.responsedata.ApiSeverity;
import ro.ut.sd.responsedata.MessageResponse;
import ro.ut.sd.service.MedicationService;
import ro.ut.sd.service.PatientService;

import javax.ws.rs.Consumes;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController()
@RequestMapping(value = "/medication", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class MedicationController {

    @Autowired
    private MedicationService medicationService;

    @Autowired
    private PatientService patientService;

    @GetMapping("/all")
    public ResponseEntity getAll() {
        List<MedicationDto> all = Mapper.mapMedicationListToMedicationDtoList(medicationService.getAll());
        return ApiResponseFactory.buildSuccessResponseMessage
                (BaseListDto.builder().message("Successfully retrieved all Medication.").list(all).build());
    }

    @GetMapping("/details/{id}")
    public ResponseEntity getById(@PathVariable("id") String id) {
        MedicationDto medicationDto = Mapper.mapMedicationToMedicationDto(medicationService.getById(id));
        return ApiResponseFactory.buildSuccessResponseMessage(medicationDto);
    }

    @PostMapping("/add")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addMedication(@RequestBody MedicationDto medicationDto) {
        Medication medication = Medication.builder()
                .dosage(medicationDto.getDosage()).name(medicationDto.getName())
                .sideEffects(medicationDto.getSideEffects()).id(UUID.randomUUID().toString())
                .build();
        medicationService.addNew(medication);
        return ApiResponseFactory.buildSuccessResponseMessage(MessageResponse.builder().severity(ApiSeverity.SUCCESS.getValue())
                .message("Successfully saved new medication " + medication.getName()).id(medication.getId()).build());
    }

    @PutMapping("/assignToPatient")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity assignMedicationToPatient(@RequestBody MedicationDto medicationDto) {
        Medication medication = medicationService.getById(medicationDto.getMedicationId());
        Patient patient = patientService.getById(medicationDto.getPatientId());
        patient.getMedicalRecord().add(medication);
        patientService.update(patient);
        return ApiResponseFactory.buildSuccessResponseMessage("Successfully assigned medication " + medication.getName() + " to patient " + patient.getName());
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity remove(@PathVariable("id") String id) {
        Medication medication = medicationService.getById(id);
        Patient patient = patientService.getByMedicineId(medication.getId());
        if (patient != null) {
            Set<Medication> patientMedicalRec = patient.getMedicalRecord();
            if (CollectionUtils.isNotEmpty(patientMedicalRec)) {
                patientMedicalRec.removeIf(p -> p.getId().equals(medication.getId()));
            }
            patientService.update(patient);
        }
        medicationService.remove(medication);
        return ApiResponseFactory.buildSuccessResponseMessage("Successfully removed medication " + medication.getName());
    }
}
