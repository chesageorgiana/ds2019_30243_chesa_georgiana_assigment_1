package ro.ut.sd.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import ro.ut.sd.responsedata.ApiResponseFactory;

import java.util.Date;
import java.util.NoSuchElementException;


@ControllerAdvice
@Slf4j
public class AppControllerAdvice {

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity handleAccessDeniedException(AccessDeniedException ex) {
        log.error("Attempt to make a request for an endpoint with an invalid role/permission.Timestamp: " + new Date(), ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.FORBIDDEN, "You do not have permission or access rights to the requested endpoint.");
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity handleExceptionStatusNotAcceptable(HttpMessageNotReadableException ex) {
        log.error("Attempt to make a request with an invalid/bad format content type.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.NOT_ACCEPTABLE, "The request type is not acceptable.");
    }

    @ExceptionHandler(OAuth2Exception.class)
    public ResponseEntity handleExceptionInvalidToken(OAuth2Exception ex) {
        log.error("Attempt to make a request with invalid token.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.UNAUTHORIZED, ex.getMessage());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity handleExceptionEndpointNotFound(NoHandlerFoundException ex) {
        log.error("Attempt to make a request for a non-existing endpoint resource.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.NOT_FOUND, "The requested endpoint location does not exist.");
    }

    @ExceptionHandler({MissingPathVariableException.class, MissingServletRequestParameterException.class})
    public ResponseEntity handleMissingPathParameters(ServletRequestBindingException ex) {
        log.error("Attempt to make a request with missing parameters.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.NOT_FOUND, "The request could not be accepted, there are missing parameters.");
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity handleExceptionStatusUnsupportedMediaType(HttpMediaTypeNotSupportedException ex) {
        log.error("Attempt to make a request a different content type than JSON", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "The Content-Type is not accepted in the application.");
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity handleExceptionStatusMethodNotAllowed(HttpRequestMethodNotSupportedException ex) {
        log.error("Attempt to make a request on a endpoint that does not support the method type", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.METHOD_NOT_ALLOWED, "The specified request method is not supported.");
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleRuntimeException(RuntimeException ex) {
        log.error("The application encountered a runtime exception.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong, please contact the development team.");
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity handlePaginationException(PropertyReferenceException ex) {
        log.error("Pagination request exception.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.BAD_REQUEST, "Something went wrong during pagination.");
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity handleNoSuchElementException(PropertyReferenceException ex) {
        log.error("No such element exception.", ex);
        return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.BAD_REQUEST, ex.getMessage());
    }
}