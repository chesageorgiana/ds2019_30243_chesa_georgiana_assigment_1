package ro.ut.sd.model;

import lombok.*;

import javax.persistence.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

import static javax.persistence.TemporalType.TIMESTAMP;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("CAREGIVER")
public class Caregiver extends User{

    @Column
    @Temporal(TIMESTAMP)
    private Date birthDate;

    @Column
    private String gender;

    @Column
    private String address;

    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    private Set<Patient> patients;

        @Builder
    public Caregiver(String id, String username, String name, String password, Date birthDate, String gender, String address, Set<Patient> patients) {
        super(id, username, name, password);
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }
}
