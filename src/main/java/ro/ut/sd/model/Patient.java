package ro.ut.sd.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

import static javax.persistence.TemporalType.TIMESTAMP;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("PATIENT")
public class Patient extends User{

    @Builder
    public Patient(String id, String username, String name, String password, Date birthDate, String gender,
                   String address, Set<Medication> medicalRecord, Caregiver caregiver, String imgUrl) {
        super(id, username, name, password);
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.imgUrl=imgUrl;
    }

    @Column
    @Temporal(TIMESTAMP)
    private Date birthDate;

    @Column
    private String gender;

    @Column
    private String address;

    @Column
    private String imgUrl;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "patient_medication",
            joinColumns = { @JoinColumn(name = "patient_id") },
            inverseJoinColumns = { @JoinColumn(name = "medication_id") })
    private Set<Medication> medicalRecord;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="caregiver_id")
    private Caregiver caregiver;
}
