package ro.ut.sd.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name="user")
public class LoginModel {

    @Id
    private String id;

    @Column
    private String username;

    @Column
    private String name;

    @Column
    private String password;

    @Column
    private String user_type;
}
