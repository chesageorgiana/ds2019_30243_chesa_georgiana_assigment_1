package ro.ut.sd.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@DiscriminatorValue("DOCTOR")
public class Doctor extends User{

    @Column
    private String email;

    @Builder
    public Doctor(String id, String username, String name, String password, String email) {
        super(id, username, name, password);
        this.email = email;
    }
}
