package ro.ut.sd.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Medication {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String sideEffects;

    @Column
    private String dosage;


}
