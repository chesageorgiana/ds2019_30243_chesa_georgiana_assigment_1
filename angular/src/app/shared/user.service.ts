import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { OauthToken } from '../OauthToken';

@Injectable()
export class UserService {

    public userCookie = this.cookie.get("token");

    constructor(private cookie: CookieService) { }


    public isUserLoggedIn(){
        if (this.cookie.check("token") === true) {
            return true;
          }
          return false;
    }

    public setLoginToken(loginToken:OauthToken){
        this.cookie.set("token",loginToken.access_token,loginToken.expires_in);
        this.cookie.set("username",loginToken.username,loginToken.expires_in);
        this.cookie.set("name",loginToken.fullname,loginToken.expires_in);
        this.cookie.set("role",loginToken.role,loginToken.expires_in);
        localStorage.setItem('token',loginToken.access_token);
    }

    public getUserToken(){
        const token= localStorage.getItem('token');
        return token;
    }


}
