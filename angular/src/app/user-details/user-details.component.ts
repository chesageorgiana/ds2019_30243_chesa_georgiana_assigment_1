import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from '../entities/ApiResponse';
import { LoginService } from '../login/login.service';
import { HttpHeaders } from '@angular/common/http';
import { SessionUser } from '../entities/SessionUser';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  constructor(private loginService:LoginService,
    private toastr: ToastrService,
    private cookieService:CookieService) { }

  private sessionUser:SessionUser;


  ngOnInit() {
    this.loginService.getSessionUser(this.cookieService.get("token")).subscribe(
        response => {
        this.sessionUser = response;
        this.toastr.info("Keep in mind that this information are private!");
      });
  }

}
