import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PersonService } from './register/register.service';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from './login/login.service';
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import 'hammerjs';
import {ApplicationService} from './app.service'
import { AppRoutingModule } from './/app-routing.module';
import { AuthGuardService } from './login/authentication.components';
import { MainPageComponent } from './main-page/main-page.component';
import { NgxPayPalModule } from 'ngx-paypal';
import {AgmCoreModule} from '@agm/core';
import { UserService } from './shared/user.service';
import { ToastrModule } from 'ngx-toastr';
import { PatientsComponent } from './patients/patients.component';
import { CaregiversComponent } from './caregivers/caregivers.component';
import { MedicationsComponent } from './medications/medications.component';
import { CaregiverService } from './caregivers/caregivers.service';
import { MedicationService } from './medications/medication.service';
import { PatientService } from './patients/patient.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { CaregiverDetailsDialogComponent } from './caregiver-details-dialog/caregiver-details-dialog.component';
import { CaregiversDeleteDialogComponent } from './caregivers/caregivers-delete-dialog';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MainPageComponent,
    PatientsComponent,
    CaregiversComponent,
    MedicationsComponent,
    UserDetailsComponent,
    PatientDetailsComponent,
    CaregiverDetailsDialogComponent,
    CaregiversDeleteDialogComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey : 'AIzaSyBGOnaqEp47SwEYKh969NF2GXuhOLJ80KQ'
    }),
    NgxPayPalModule,
    BrowserModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    FormsModule,
    DlDateTimePickerDateModule,
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    AppRoutingModule,
    ToastrModule.forRoot() // ToastrModule added

  ],
  entryComponents: [PatientDetailsComponent,CaregiverDetailsDialogComponent,CaregiversDeleteDialogComponent],
  providers: [
    ApplicationService,PersonService,
    LoginService,CookieService,FormsModule,
    CaregiverService,MedicationService,PatientService,
    AuthGuardService,LoginComponent, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
