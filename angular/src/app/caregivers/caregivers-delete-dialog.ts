import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CaregiverService } from './caregivers.service';
import { ApiResponse } from '../entities/ApiResponse';

@Component({
  templateUrl: './caregivers-delete-dialog.html',
})
export class CaregiversDeleteDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CaregiversDeleteDialogComponent>,
    private toastr: ToastrService,
    private caregiverService: CaregiverService,
    @Inject(MAT_DIALOG_DATA) public data: ApiResponse) { }

  onClose(): void {
    this.dialogRef.close();
  }

  private apiResponse: ApiResponse;

  confirm(): void {
     this.caregiverService.deleteById(this.data.id).subscribe(
      (response: ApiResponse) => {
        this.apiResponse = response;
        this.dialogRef.close();
        this.toastr.warning(response.message, "Caregiver deleted");
      },
      error => {
        if (error) {
          this.apiResponse = error.error;
          this.toastr.error(this.apiResponse.error_description);
        }
      }); 
  }

  exit(): void {
    this.dialogRef.close();
    this.toastr.info("You changed your mind so caregiver was not deleted.", "Caregiver not deleted");
  }

}
