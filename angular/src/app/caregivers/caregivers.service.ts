import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../shared/api-url';
import { Caregiver } from '../entities/Caregiver';

@Injectable()
export class CaregiverService {

    constructor(private http: HttpClient) { }
    
    getAll<CaregiverList>(): Observable<CaregiverList> {
        return this.http.get<CaregiverList>(API.URL + "caregiver/all");
    }

    getById<Caregiver>(id:String): Observable<Caregiver> {
        return this.http.get<Caregiver>(API.URL + "caregiver/details/"+id);
    }

    deleteById<ApiResponse>(id:String): Observable<ApiResponse> {
        return this.http.delete<ApiResponse>(API.URL + "caregiver/remove/"+id);
    }

    addNewCaregiver<ApiResponse>(caregiver:Caregiver): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(API.URL + "caregiver/add",caregiver);
    }
    
}