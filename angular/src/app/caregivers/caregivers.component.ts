import { Component, OnInit } from '@angular/core';
import { CaregiverService } from './caregivers.service';
import { Caregiver } from '../entities/Caregiver';
import { CaregiverList } from '../entities/CaregiverList';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { CaregiverDetailsDialogComponent } from '../caregiver-details-dialog/caregiver-details-dialog.component';
import { CaregiversDeleteDialogComponent } from './caregivers-delete-dialog';

@Component({
  selector: 'app-caregivers',
  templateUrl: './caregivers.component.html',
  styleUrls: ['./caregivers.component.css']
})
export class CaregiversComponent implements OnInit {

  constructor(public dialog: MatDialog, private cgService: CaregiverService, private toastr: ToastrService) { }

  public allCaregivers: Caregiver[];
  public caregiverDetails: Caregiver;
  private confirmDialogResult:String;

  ngOnInit() {
    this.cgService.getAll().subscribe((data: CaregiverList) => this.allCaregivers = data.list);
  }

  viewDetails(id: string) {
    this.cgService.getById(id).subscribe((data: Caregiver) => this.caregiverDetails = data);

    const dialogRef = this.dialog.open(CaregiverDetailsDialogComponent, {
      width: '350px', data: {
        username: this.caregiverDetails.username,
        id: this.caregiverDetails.id,
        birthDate: this.caregiverDetails.birthDate,
        gender: this.caregiverDetails.gender,
        patients: this.caregiverDetails.patients
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  deleteById(id: string) {
    const dialogRef = this.dialog.open(CaregiversDeleteDialogComponent, {
      width: '350px', data: {
        id: id
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.confirmDialogResult=result;
      console.log( this.confirmDialogResult);
      if ( this.confirmDialogResult = "true") {
        this.allCaregivers = this.allCaregivers.filter(caregiver => caregiver.id != id);
      }
    });

  }

}
