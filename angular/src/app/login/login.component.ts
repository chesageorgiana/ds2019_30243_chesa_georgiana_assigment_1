import { Component, OnInit } from '@angular/core';
import { Person } from '../Person';
import { LoginService } from './login.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { OauthToken } from '../OauthToken';
import { ApiResponse } from '../entities/ApiResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public person = {} as Person;
  public loginToken : OauthToken;
  private errorResponse: ApiResponse;
  returnUrl: string;
  public token;

  constructor(private loginService: LoginService,
    private cookie: CookieService, private route: ActivatedRoute,
    private router: Router, private userService: UserService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login(person): void {
    this.loginService.login(person).subscribe(
      response => {
        this.loginToken = response;
        this.router.navigateByUrl('home');
        this.userService.setLoginToken(response);
        this.toastr.success("Login success.");
        this.toastr.info("Welcome "+response.fullname+".");
      },
      error => {
        console.log(error);
        if (error) {
          this.errorResponse=error.error;
          this.toastr.error(this.errorResponse.error_description);
        }
        else {
          this.toastr.error( 'Something wrong happened');
        }
      }
    );
  }

}
