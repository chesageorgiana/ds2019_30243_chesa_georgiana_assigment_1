import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../shared/user.service';
import { LoginComponent } from './login.component';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor( private cookie: CookieService, private router: Router, private uservice: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.uservice.isUserLoggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}