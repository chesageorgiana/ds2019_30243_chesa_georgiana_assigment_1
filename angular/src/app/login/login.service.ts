import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Person } from '../Person';
import { OauthToken } from '../OauthToken';
import { API } from '../shared/api-url';
import { CookieService } from 'ngx-cookie-service';
import { ApiResponse } from '../entities/ApiResponse';
import { SessionUser } from '../entities/SessionUser';

@Injectable()
export class LoginService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic dXRjblNEbGFiOnV0Y25MQUJjbHBhc3N3b3Jk'
        })
    };
  
    authorizationHeaders = {
        headers: new HttpHeaders({
            'Authorization': 'Bearer ' + this.cookieService.get("token")
        })
          };


    constructor(private http: HttpClient,private cookieService:CookieService) { }

    login(person: Person): Observable<OauthToken> {
        return this.http.post<OauthToken>(API.URL + "oauth/token?grant_type=password&username=" + person.username + "&" + "password=" + person.password,
            person, this.httpOptions);
    }

    getSessionUser(token: string): Observable<SessionUser> {
        return this.http.post<SessionUser>(API.URL + "auth/session?token=" + token,null, this.authorizationHeaders);
    }

}
