import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from './shared/api-url';

@Injectable()
export class ApplicationService {
    constructor(private http: HttpClient) { }
    
    logout(token:string): Observable<String>  {
    return  this.http.get<String>(API.URL + "auth/remove/"+token,{ withCredentials: true });
    }
}
