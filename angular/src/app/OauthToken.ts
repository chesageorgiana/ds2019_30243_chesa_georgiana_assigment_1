export interface OauthToken {
    access_token: string;
    expires_in: number;
    severity: string;
    role: string;
    fullname: string;
    username: string;
  }
  