import { Component, OnInit } from '@angular/core';
import { Medication } from '../entities/Medication';
import { MedicationService } from './medication.service';
import { MedicationList } from '../entities/MedicationList';
import { ApiResponse } from '../entities/ApiResponse';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-medications',
  templateUrl: './medications.component.html',
  styleUrls: ['./medications.component.css']
})
export class MedicationsComponent implements OnInit {

  constructor(private medService: MedicationService, private toastr: ToastrService) { }

  public allMedications: Medication[];
  private apiResponse: ApiResponse;

  ngOnInit() {
    this.medService.getAll().subscribe((data: MedicationList) => this.allMedications = data.list);
  }

  deleteMedicationById(id: string) {
    this.medService.deleteById(id)
      .subscribe((data: ApiResponse) => { this.apiResponse = data 
        this.toastr.success(data.message);      
        this.allMedications = this.allMedications.filter(med => med.id != id);
      },
        error => {
          this.toastr.error("Medication remove error");
        }
      );
  }

}
