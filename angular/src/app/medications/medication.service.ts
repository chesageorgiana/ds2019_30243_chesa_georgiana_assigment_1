import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../shared/api-url';
import { Medication } from '../entities/Medication';

@Injectable()
export class MedicationService {

    constructor(private http: HttpClient) { }
    
    getAll<MedicationList>(): Observable<MedicationList> {
        return this.http.get<MedicationList>(API.URL + "medication/all");
    }

    getById<Medication>(id:String): Observable<Medication> {
        return this.http.get<Medication>(API.URL + "medication/details/"+id);
    }
    
    deleteById<ApiResponse>(id:String): Observable<ApiResponse> {
        return this.http.delete<ApiResponse>(API.URL + "medication/remove/"+id);
    }

    addNewMedication<ApiResponse>(medication:Medication): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(API.URL + "medication/add",medication);
    }

    assignMedicationToPatient<ApiResponse>(medication:Medication): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(API.URL + "medication/add",medication);
    }
}