import { Medication } from "./Medication";

export interface MedicationList {

    message: string;
    list:Array<Medication>;  

 }