import { Caregiver } from "./Caregiver";

export interface CaregiverList {

    message: string;
    list:Array<Caregiver>;  

 }