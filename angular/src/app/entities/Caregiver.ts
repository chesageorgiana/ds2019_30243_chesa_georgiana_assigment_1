import { Patient } from "./Patient";

export interface Caregiver {
    id : string ;
    name: string;
    birthDate: string;
    gender: string;
    address: string;
    imgUrl:string;
    username:string;
    patients:Array<Patient>;  
    message: string;
 }