import { Caregiver } from "./Caregiver";

export interface Patient {
    id : string ;
    name: string;
    username: string;
    password: string;
    birthDate: string;
    gender: string;
    address: string;
    imgUrl:string;
    medicalRecord:Array<String>;  
    caregiver:Caregiver;
    message: string;
 }