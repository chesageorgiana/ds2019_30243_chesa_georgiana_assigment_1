export interface ApiResponse {
    id : string ;
    message: string;
    severity: string;
    error:string;
    error_description:string;
 }