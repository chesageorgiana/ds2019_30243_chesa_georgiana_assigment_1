export interface Medication {
     id : string ;
     name: string;
     sideEffects: string;
     dosage: string;
     message: string;
     patientId: string;
     medicationId: string;
  }