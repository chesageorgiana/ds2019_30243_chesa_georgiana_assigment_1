import { Patient } from "./Patient";

export interface PatientList {

    message: string;
    list:Array<Patient>;  

 }