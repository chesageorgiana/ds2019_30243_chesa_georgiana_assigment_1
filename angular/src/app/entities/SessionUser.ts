import { Caregiver } from "./Caregiver";

export interface SessionUser {
    id: string;
    name: string;
    user_type: string;
    birthDate: string;
    password:string;
    gender:string;
    address:string;
    imgUrl:string;
    medicalRecord:Array<String>;  
    caregiver:Caregiver;
}