import { Component, OnInit } from '@angular/core';
import { PersonService } from './register.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Patient } from '../entities/Patient';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  private patient = {} as Patient;
  constructor(private personService: PersonService,
  private toastr: ToastrService,
  private router: Router) { }

  ngOnInit() {}

  addPatient(patient): void {
    this.personService.addPatient(patient).subscribe(response => {
    this.toastr.success("Successfull registration! Please go to login page");
    setTimeout(() =>
    {
        this.router.navigate(['/login']);
    },
    600);
    },
      response => {
        console.log("Error", response);
        if (response.error.message){
          this.toastr.error(response.error.message);
        }
        else{
          this.toastr.error("Something went wrong! Please try again!");
        }

      }
    );
  }



}
