import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../shared/api-url';
import { Patient } from '../entities/Patient';

@Injectable()
export class PersonService {

    constructor(private http: HttpClient) { }
    
    addPatient(patient: Patient): Observable<Patient> {
        return this.http.post<Patient>(API.URL + "patient/add", patient,{ withCredentials: true });
    }
}
