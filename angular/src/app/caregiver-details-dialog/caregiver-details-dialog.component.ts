import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Caregiver } from '../entities/Caregiver';


@Component({
  selector: 'app-caregiver-details-dialog',
  templateUrl: './caregiver-details-dialog.component.html',
  styleUrls: ['./caregiver-details-dialog.component.css']
})
export class CaregiverDetailsDialogComponent{

  constructor(
    public dialogRef: MatDialogRef<CaregiverDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public caregiver: Caregiver) { }

    exit(): void {
      this.dialogRef.close();
    }
}
