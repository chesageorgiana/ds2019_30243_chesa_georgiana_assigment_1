import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverDetailsDialogComponent } from './caregiver-details-dialog.component';

describe('CaregiverDetailsDialogComponent', () => {
  let component: CaregiverDetailsDialogComponent;
  let fixture: ComponentFixture<CaregiverDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
