import { Component, OnInit } from '@angular/core';
import { PatientService } from './patient.service';
import { Patient } from '../entities/Patient';
import { PatientList } from '../entities/PatientList';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { PatientDetailsComponent } from '../patient-details/patient-details.component';


@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {

  constructor(private patService: PatientService,
    private toastr: ToastrService,
    public dialog: MatDialog) { }

  public patientDetails: Patient;
  public allPatients: Patient[];

  ngOnInit() {
    this.patService.getAll().subscribe((data: PatientList) => this.allPatients = data.list);
  }

  viewDetails(id: string) {

    this.patService.getById(id).subscribe((data: Patient) => this.patientDetails = data);

    const dialogRef = this.dialog.open(PatientDetailsComponent, {
      width: '350px',data:{ name:this.patientDetails.name,
                            id:this.patientDetails.id,
                            medicalRecord:this.patientDetails.medicalRecord,
                            birthDate:this.patientDetails.birthDate,
                            gender:this.patientDetails.gender,
                            username:this.patientDetails.username,
                            caregiver:this.patientDetails.caregiver
                          }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }




}
