import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../shared/api-url';
import { Patient } from '../entities/Patient';

@Injectable()
export class PatientService {

    constructor(private http: HttpClient) { }
    
    getAll<PatientList>(): Observable<PatientList> {
        return this.http.get<PatientList>(API.URL + "patient/all");
    }

    getById<Patient>(id:String): Observable<Patient> {
        return this.http.get<Patient>(API.URL + "patient/details/"+id);
    }

    deleteById<ApiResponse>(id:String): Observable<ApiResponse> {
        return this.http.delete<ApiResponse>(API.URL + "patient/remove/"+id);
    }

    addNewPatient<ApiResponse>(patient:Patient): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(API.URL + "caregiver/add",patient);
    }
}