import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router, RouterOutlet } from '@angular/router';
import { ApplicationService } from './app.service'
import { UserService } from './shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { OauthToken } from './OauthToken';
import { LoginComponent } from './login/login.component';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'Medical app';

  private loginToken = {} as OauthToken;

  constructor(
    private appService: ApplicationService,
    private loginComponent: LoginComponent,
    private cookieService: CookieService,
    private router: Router,
    public userService: UserService,
    private toastr: ToastrService) { }

  public today = new Date();

  ngOnInit() {
    this.loginToken = this.loginComponent.loginToken;
  }

  isLoggedIn(): boolean {
    if (this.cookieService.check("token") === true) {
      return true;
    }
    return false;
  }

  getAnimationData(routerOutlet: RouterOutlet) {
    const routeData = routerOutlet.activatedRouteData['animation'];
    return routeData ? routeData : 'rootPage';
  }

  canLogOut(): boolean {
    if (this.cookieService.check("token") == false) {
      return true;
    }
    return false;
  }

  navToPatients(): void {
    this.router.navigate(['/patients']);
  }
  navToCaregivers(): void {
    this.router.navigate(['/caregivers']);
  }
  navToMedications(): void {
    this.router.navigate(['/medications']);
  }

  navigateToLogin(): void {
    this.router.navigate(['/login']);
  }
  navigateToHome(): void {
    this.router.navigate(['/home']);
  }

  navigateToPersonalDetails(): void {
    this.router.navigate(['/user/details']);
  }


  logout() {
    this.appService.logout("token").subscribe(response => {
      this.toastr.success('Logout successful');
      this.cookieService.delete("token");
      this.router.navigate(['/home']);
    })
  }

}

