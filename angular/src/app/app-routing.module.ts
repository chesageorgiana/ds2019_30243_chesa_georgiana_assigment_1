import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { RegisterComponent } from './register/register.component'
import { AuthGuardService as AuthGuard } from './login/authentication.components';
import { MainPageComponent } from './main-page/main-page.component';
import { PatientsComponent } from './patients/patients.component';
import { CaregiversComponent } from './caregivers/caregivers.component';
import { MedicationsComponent } from './medications/medications.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';

const routes: Routes = [

  { path: 'login', component: LoginComponent },
  { path: 'patient-details', component: PatientDetailsComponent },
  { path: 'user/details', component: UserDetailsComponent },
  { path: '', component: MainPageComponent, },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: MainPageComponent, },
  { path: 'patients', component: PatientsComponent, canActivate: [AuthGuard] },
  { path: 'caregivers', component: CaregiversComponent, canActivate: [AuthGuard] },
  { path: 'medications', component: MedicationsComponent,canActivate: [AuthGuard]  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: [],
})

export class AppRoutingModule {

}
